'use strict';

let util = require('./util');
let db = require('./db');

let defaultOptions = {
    checkExpirationInterval: 2 * 60 * 60 * 1000,// How frequently expired sessions will be cleared; milliseconds; default 2 hour.
    expiration: 24 * 60 * 60 * 1000,// The maximum age of a valid session; milliseconds; default 1 day.
    createDatabaseTable: true,// Whether or not to create the sessions database table, if one does not already exist.
    schema: {
        tableName: 'sessions',
        columnNames: {
            session_id: 'session_id',
            expires: 'expires',
            data: 'attributes'
        }
    }
};

module.exports = (session) => {
    let Store = session.Store;
    let noop = () => {};

    class OracleStore extends Store {
        constructor(options = {}, connection) {
            super(options)
            if (!connection) {
                throw new Error('A Oracle connection must be directly provided to the OracleStore')
            }
            this.connection = connection;
            this.opts = util.deepMergeObjs(defaultOptions, options);
            if (this.opts.createDatabaseTable) {
                db.createSessTable(this.connection, this.opts, (err) => {
                    this.opts.createDatabaseTable = false;
                });
            }
            /// remove expired sessions
            setInterval(() => {
                return db.clearExpiredSessions(this.connection, this.opts, noop);
                }, this.opts.checkExpirationInterval)
        }

        /// (Required) set a session from the store given a session ID (sid).
        /// The callback should be called as callback(error, session).
        get(sid, cb = noop) {
            return db.getSession(sid, this.connection, this.opts, cb);
        }

        /// (Required) upsert a session into the store given a session ID (sid) and session (session) object.
        /// The callback should be called as callback(error) once the session has been set in the store.
        set(sid, sess, cb = noop) {
            return db.setSession(sid, sess, this.connection, this.opts, cb);
        }

        /// (Recommended) "touch" a given session given a session ID (sid) and session (session) object.
        /// The callback should be called as callback(error) once the session has been touched.
        touch(sid, sess, cb = noop) {
            return db.touchSession(sid, sess, this.connection, this.opts, cb);
        }

        /// (Required) destroy/delete a session from the store given a session ID (sid).
        /// The callback should be called as callback(error) once the session is destroyed.
        destroy(sid, cb = noop) {
            return db.destroySession(sid, this.connection, this.opts, cb);
        }

        /// (Optional) delete all sessions from the store.
        /// The callback should be called as callback(error) once the store is cleared.
        clear(cb = noop) {
            return db.clearSessions(this.connection, this.opts, cb);
        }

        /// (Optional) get the count of all sessions in the store.
        /// The callback should be called as callback(error, len).
        length(cb = noop) {
            return db.length(this.connection, this.opts, cb);
        }

        /// (Optional) get all sessions in the store as an array.
        /// The callback should be called as callback(error, sessions).
        all(cb = noop) {
            return db.getAllSessions(this.connection, this.opts, cb);
        }

    }

    return OracleStore;
};