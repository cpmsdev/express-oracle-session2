'use strict';
let oracledb = require('oracledb');

let getExpiresVal = (sess, opts) => {
    let expires;
    if (sess.cookie) {
        if (sess.cookie.expires) {
            expires = sess.cookie.expires;
        } else if (sess.cookie._expires) {
            expires = sess.cookie._expires;
        }
    }

    if (!expires) {
        expires = Date.now() + opts.expiration; // extend expires time
    }
    if (!(expires instanceof Date)) {
        expires = new Date(expires);
    }

    // use seconds here, not milliseconds
    expires = Math.round(expires.getTime() / 1000);

    return expires;
};

/// return callback(error, session).
let getSessFromOracle = (sid, connection, opts, cb) => {
    let params = {
        sessionid: sid
    };
    let sql = `SELECT ${opts.schema.columnNames.data} AS data 
                FROM ${opts.schema.tableName} 
                WHERE ${opts.schema.columnNames.session_id} = :sessionid 
                AND ROWNUM = 1`;
    return connection.execute(sql, params,
        { fetchInfo: { DATA: { type: oracledb.STRING}}, outFormat: oracledb.OUT_FORMAT_OBJECT},
        (err, res) => {
        if (err) {
            console.error('Failed to get session.');
            console.error(err);
            return cb && cb(err, null);
        }
        let session = null;

        try {
            session = res.rows[0] && JSON.parse(res.rows[0].DATA);
        } catch (e) {
            console.error(e);
            return cb && cb(new Error(`Failed to parse data for session: + ${sid}`), null);
        }

        return cb && cb(null, session);
    });

};

/// return callback(error, sessions)
let getAllSessFromOracle = (connection, opts, cb) => {
    let sql = `SELECT ${opts.schema.columnNames.data} AS data 
                FROM ${opts.schema.tableName}`;

    connection.execute(sql, {},
        { fetchInfo: { DATA: { type: oracledb.STRING}}, outFormat: oracledb.OUT_FORMAT_OBJECT},
        (err, res) => {
        if (err) {
            console.error('Failed to get all sessions.');
            console.error(err);
            return cb && cb(err);
        }

        let sessions = [];
        try {
            if (res.rows && res.rows.length > 0) {
                sessions = res.rows.map((obj) => {
                    return JSON.parse(obj.DATA);
                });
            }

        } catch (e) {
            console.error(e);
            return cb && cb(new Error('Failed to parse data for session: ' + sid));
        }
        return cb && cb(null, sessions);
    });
};

/// return callback(error)
let clearExpiredSessInOracle = (connection, opts, cb) => {
    let params = {
        expired: Math.round(Date.now() / 1000)
    };
    let sql = `DELETE FROM ${opts.schema.tableName} 
    WHERE ${opts.schema.columnNames.expires} < :expired`;

    connection.execute(sql, params, {autoCommit: true}, (err, results) => {
        if (err) {
            console.error('Failed to clear expired sessions.');
            console.error(err);
            return cb && cb(err);
        }
        // console.log(`Removed ${results.rowsAffected} expired sessions.`);
        cb && cb(null);
    });
};

/// callback(error)
let touchSessInOracle = async (sid, sess, connection, opts, cb) => {
    let params = {
        expires: getExpiresVal(sess, opts),
        sessionid: sid
    };
    let sql = `UPDATE ${opts.schema.tableName} 
    SET ${opts.schema.columnNames.expires} = :expires 
    WHERE ${opts.schema.columnNames.session_id} = :sessionid`;

    connection.execute(sql, params, {autoCommit: true}, (err, results) => {
        if (err) {
            console.error('Failed to touch session.');
            console.error(err);
            return cb && cb(err);
        }

        // console.log(`Touched ${results.rowsAffected} session.`);
        return cb && cb(null);
    });
};

/// return callback(error)
let setSessToOracle = async (sid, sess, connection, opts, cb) => {
    let params = {
        sessionid: sid,
        expires: getExpiresVal(sess, opts),
        attributes: JSON.stringify(sess)
    };
    let sql = `MERGE INTO ${opts.schema.tableName} trg
                USING (SELECT :sessionid as ${opts.schema.columnNames.session_id} FROM DUAL) src
                ON (trg.${opts.schema.columnNames.session_id} = src.${opts.schema.columnNames.session_id})
                WHEN MATCHED THEN UPDATE
                SET trg.${opts.schema.columnNames.expires} = :expires,
                trg.${opts.schema.columnNames.data} = :attributes
                WHEN NOT MATCHED THEN INSERT (${opts.schema.columnNames.session_id}, ${opts.schema.columnNames.expires}, ${opts.schema.columnNames.data})
                VALUES (:sessionid,:expires,:attributes)`;
    return connection.execute(sql, params, {autoCommit: true}, (err, results) => {
        if (err) {
            console.error('Failed to insert session data.');
            console.error(err);
            return cb && cb(err);
        }
        // console.log(`Set ${results.rowsAffected} session.`)
        return cb && cb(null);
    });
};

/// return callback(error)
let destroySessInOracle = (sid, connection, opts, cb) => {
    let params = {
        sessionid: sid
    };
    let sql = `DELETE FROM ${opts.schema.tableName} 
                WHERE ${opts.schema.columnNames.session_id} = :sessionid`;

    connection.execute(sql, params, {autoCommit: true}, (err, results) => {
        if (err) {
            console.error('Failed to destroy session.');
            console.error(err);
            return cb && cb(err);
        }

        // console.log(`Destroyed ${results.rowsAffected} sessions.`);
        return cb && cb(null);
    });
};

/// return callback(error)
let clearSessInOracle = (connection, opts, cb) => {
    let sql = `TRUNCATE TABLE ${opts.schema.tableName}`;

    connection.execute(sql, {}, {autoCommit: true}, (err) => {
        if (err) {
            console.error('Failed to clear all sessions.');
            console.error(err);
            return cb && cb(err);
        }

        // console.log(`Cleared all sessions.`);
        return cb && cb(null);
    });
};

/// return callback(error, len)
let getCount = (connection, opts, cb) => {
    let sql = `SELECT COUNT(*) as count FROM ${opts.schema.tableName}`;
    connection.execute(sql, {}, {outFormat: oracledb.OUT_FORMAT_OBJECT},
        (err, result) => {
        if (err) {
            console.error('Failed to get number of sessions.');
            console.error(err);
            return cb && cb(err);
        }
        let count = result.rows[0] ? result.rows[0].COUNT : 0;
        return cb && cb(null, count);
    });
};

/// return callback(error)
let createSessTable = (connection, opts, cb) => {
    let sql = `CREATE TABLE ${opts.schema.tableName} (
                  ${opts.schema.columnNames.session_id} VARCHAR(128)  NOT NULL,
                  ${opts.schema.columnNames.expires} NUMBER(11) NOT NULL,
                  ${opts.schema.columnNames.data} CLOB,
                  PRIMARY KEY (${opts.schema.columnNames.session_id})
                )`;

   return connection.execute(sql, {}, {autoCommit: true}, (err) => {
            if (err) {
                //console.log(error.message)
                if(err.message.substring(0,9) === 'ORA-00955'){
                    return cb();
                }
                return cb && cb(err);
            }
            return cb && cb(null);
        });
};


module.exports = {
    getSession: getSessFromOracle,
    setSession: setSessToOracle,
    touchSession: touchSessInOracle,
    destroySession: destroySessInOracle,
    createSessTable: createSessTable,
    length: getCount,
    clearSessions: clearSessInOracle,
    getAllSessions: getAllSessFromOracle,
    clearExpiredSessions: clearExpiredSessInOracle
};