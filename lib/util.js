/// Merge multiple objects, the first object is updated with later objects
/// refer to this solution:
/// https://medium.com/javascript-in-plain-english/how-to-merge-objects-in-javascript-98f2209710e3
let deepMergeObjs = (...args) => {
    let target = {};
    // Merge the object into the target object
    let merger = (obj) => {
        for (let prop in obj) {
            if (obj.hasOwnProperty(prop)) {
                // typeof will identify null as 'object'
                // if you want to use typeof operator instead, the condition should be
                // typeof obj[prop] === 'object' && obj[prop] != null
                if (Object.prototype.toString.call(obj[prop]) === '[object Object]'){
                    // If we're doing a deep merge
                    // and the property is an object
                    target[prop] = deepMergeObjs(target[prop], obj[prop]);
                } else {
                    // Otherwise, do a regular merge
                    target[prop] = obj[prop];
                }
            }
        }
    };
    //Loop through each object and conduct a merge
    for (let arg of args ) {
        merger(arg);
    }
    return target;
};

module.exports = {
    deepMergeObjs: deepMergeObjs
};