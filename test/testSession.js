'use strict';

let expect  = require("chai").expect;
let oracledb = require('oracledb');
let session = require('express-session');
let config = require('../config');

/// initialize Oracle client
try {
    oracledb.initOracleClient({libDir: config.oraClientDir});
} catch (e) {
    console.error(e);
    process.exit(1);
}

describe('Test Oracle DB Session Operations', () => {
    let db = require('../lib/db');
    let oracleDbStore = require('../lib/session')(session);
    let oraConnection, sessionStore, storeOpts,
        testSid = 'TEST4n8zxI-f4ffpWeV-OLVQqHHfTEST',
        testSess = {"cookie":{"originalMaxAge":86400000,"expires":"2021-10-10T20:52:42.552Z","httpOnly":false,"path":"/"}};

    before(async () => {
        try {
            oraConnection = await oracledb.getConnection(config.dbConfig);
            let sessOpts = {
                checkExpirationInterval: 60 * 1000, // How frequently expired sessions will be cleared; milliseconds.
                // expiration: 1000 * 60 * 60, // The maximum age of a valid session; milliseconds. It will be overwritten by maxAge in the session configuration
                createDatabaseTable: true,
                schema: {
                    tableName: 'example_sessions_test'
                }
            };
            sessionStore = new oracleDbStore(sessOpts, oraConnection);
            storeOpts = sessionStore.opts;
        } catch (e) { console.error(e); }
    });

    it('can add/refresh a session in Oracle', (done) => {
        db.setSession(testSid, testSess, oraConnection, storeOpts, (err) => {
            expect(err).to.be.null;
            let sql = `SELECT ${storeOpts.schema.columnNames.session_id} AS sid
                FROM ${storeOpts.schema.tableName} 
                WHERE ${storeOpts.schema.columnNames.session_id} = '${testSid}'`;
            oraConnection.execute(sql, {}, {outFormat: oracledb.OUT_FORMAT_OBJECT}, (e, res) => {
                expect(e).to.be.null;
                expect(res).to.have.nested.property('rows[0].SID');
                done();
            });
        });
    });

    it('can get a session from Oracle', (done) => {
        db.getSession(testSid, oraConnection, storeOpts, (err, results) => {
            expect(err).to.be.null;
            if (results.length > 0) {
                expect(results[0]).to.have.nested.property('cookie.expires');
                expect(results[0]).to.have.nested.property('cookie.originalMaxAge');
            }
            done();
        });
    });

    it('can list all sessions in Oracle', (done) => {
        /// test "all" store function
        db.getAllSessions(oraConnection, storeOpts, (err, results) => {
            expect(err).to.be.null;
            if (results.length > 0) {
                expect(results[0]).to.have.nested.property('cookie.expires');
                expect(results[0]).to.have.nested.property('cookie.originalMaxAge');
            }
            done();
        });
    });

    it('can get total number of sessions in Oracle', (done) => {
        db.length(oraConnection, storeOpts, (err, count) => {
            expect(err).to.be.null;
            expect(count).to.be.a('number');
            console.log('Total Count of Sessions:', count);
            done();
        });
    });

    it('can touch a session in Oracle', (done) => {
        db.touchSession(testSid, testSess, oraConnection, storeOpts, (err) => {
            expect(err).to.be.null;
            let sql = `SELECT ${storeOpts.schema.columnNames.expires} AS expires
                FROM ${storeOpts.schema.tableName} 
                WHERE ${storeOpts.schema.columnNames.session_id} = '${testSid}'`;
            oraConnection.execute(sql, {}, {outFormat: oracledb.OUT_FORMAT_OBJECT}, (e, res) => {
                expect(e).to.be.null;
                expect(res).to.have.nested.property('rows[0].EXPIRES');
                expect(new Date(res.rows[0].EXPIRES)).to.not.equal(new Date(testSess.cookie.expires));
                console.log('Old Expires:', new Date(testSess.cookie.expires));
                console.log('New Expires:', new Date(res.rows[0].EXPIRES * 1000));
                done();
            });
        });
    });

    it('can remove expired sessions in Oracle', (done) => {
        /// add an expired session
        let thisTestSid = 'EXPRTESTxI-f4ffpWeV-OLVQqHHfTEST';
        let thisTestSess = {"cookie":{"originalMaxAge":86400000,"expires":"2020-10-07T20:52:42.552Z","httpOnly":false,"path":"/"}};
        db.setSession(thisTestSid, thisTestSess, oraConnection, storeOpts, (err) => {
            db.clearExpiredSessions(oraConnection, storeOpts, (err) => {
                expect(err).to.be.null;
                let sql = `SELECT ${storeOpts.schema.columnNames.session_id} AS sid
                FROM ${storeOpts.schema.tableName} 
                WHERE ${storeOpts.schema.columnNames.session_id} = '${thisTestSid}'`;
                oraConnection.execute(sql, (e, res) => {
                    expect(res.rows).is.an('array').to.have.lengthOf(0);
                    done();
                });
            });
        });
    });

    it('can destroy a session in Oracle', (done) => {
        let thisTestSid = 'DESTTESTxI-f4ffpWeV-OLVQqHHfTEST';
        let thisTestSess = {"cookie":{"originalMaxAge":86400000,"expires":"2020-10-08T20:52:42.552Z","httpOnly":false,"path":"/"}};
        db.setSession(thisTestSid, thisTestSess, oraConnection, storeOpts, (err) => {
            db.destroySession(thisTestSid, oraConnection, storeOpts, (err, results) => {
                expect(err).to.be.null;
                let sql = `SELECT ${storeOpts.schema.columnNames.session_id} AS sid
                FROM ${storeOpts.schema.tableName} 
                WHERE ${storeOpts.schema.columnNames.session_id} = '${thisTestSid}'`;
                oraConnection.execute(sql, (e, res) => {
                    expect(res.rows).is.an('array').to.have.lengthOf(0);
                    done();
                });
            });
        });
    });

    it('can remove all sessions in Oracle', (done) => {
        let thisTestSid = 'CLEATESTxI-f4ffpWeV-OLVQqHHfTEST';
        let thisTestSess = {"cookie":{"originalMaxAge":86400000,"expires":"2020-10-08T20:52:42.552Z","httpOnly":false,"path":"/"}};
        db.setSession(thisTestSid, thisTestSess, oraConnection, storeOpts, (err) => {
            db.clearSessions(oraConnection, storeOpts, (err) => {
                expect(err).to.be.null;
                let sql = `SELECT count(*) as count
                FROM ${storeOpts.schema.tableName}`;
                oraConnection.execute(sql, (e, res) => {
                    expect(res.rows[0][0]).to.equal(0);
                    done();
                });
            });
        });
    });

});