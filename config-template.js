let oraClientDir = '/path/to/instantclient_19_3/';
let dbConfig = {
        alias: '',
        user: '',
        password: '',
        connectString: ''
    };

module.exports = {
    dbConfig: dbConfig,
    oraClientDir: oraClientDir
};