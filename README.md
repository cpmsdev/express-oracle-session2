# express-oracle-session2 #

This library provides Oracle session storage for Express. It is built based on [express-oracle-session](https://github.com/Slumber86/express-oracle-session.git).

Compatible with [node-oracledb](https://oracle.github.io/node-oracledb/) 5.0.0. 

### Installation ###

* Install prerequisites of [node-oracledb](https://oracle.github.io/node-oracledb/)
* `npm install express-oracle-session2`

### Configuration ###

* Need to set configuration for testings and example
* Edit _config-template.js_ as _config.js_ and configure parameters for testings

### Example ###

* Start with `npm run example`
* Open link http://127.0.0.1:3001/
* Check if a new session is stored in Oracle table _EXAMPLE_SESSIONS_TEST_ 
* Edit session config in _example/example.js_ testing different scenarios

### Tests ###

* Only test database functions in _db.js_
* Run `npm test`

### Reference  ###

* [express-oracle-session](https://github.com/Slumber86/express-oracle-session.git)