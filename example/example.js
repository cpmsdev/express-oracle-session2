'use strict';

let oracledb = require('oracledb');
let express = require('express');
let app = express();
let session = require('express-session');

let oracleDbStore = require('../lib/session')(session);
let config = require('../config');
let port = 3001;

/// initialize Oracle client
try {
    oracledb.initOracleClient({libDir: config.oraClientDir});
} catch (e) {
    console.error(e);
    process.exit(1);
}

/// start a temporary server
let startServer = (sess) => {
    app.use(sess);

    app.get('/', (req, res) => {
        res.send('Hello World!')
    })

    app.listen(port, () => {
        console.log(`Example app listening at http://127.0.0.1:${port}`);
    })
};

/// use Oracle to store expressjs session
(async () => {
    try {
        let oraConnection = await oracledb.getConnection(config.dbConfig);
        let sessOpts = {
            checkExpirationInterval: 60 * 1000, // How frequently expired sessions will be cleared; milliseconds.
            // expiration: 30 * 1000, // The maximum age of a valid session; milliseconds. It will be overwritten by maxAge in the following session.cookie configuration
            createDatabaseTable: true,
            schema: {
                tableName: 'example_sessions_test'
            }
        };
        let sessionStore = new oracleDbStore(sessOpts, oraConnection);

        let sessConfig = {
            secret: 'example secret',
            resave: false,
            saveUninitialized: true, /// Force to store an uninitialized session, since we need a session record to update after login
            rolling: true, /// Force a session expiration countdown to be reset on every response
            cookie : {
                httpOnly: false,
                maxAge: 2 * 60 * 1000 /// 2 min maxAge in milliseconds;
            },
            store: sessionStore
        };
        startServer(session(sessConfig));
    } catch (e) { console.error(e); }
})();